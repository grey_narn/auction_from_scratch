#ifndef __CUDACC__
#define LOG_AUCTION
#else
#undef LOG_AUCTION
#endif

#ifndef __CUDACC__
#include "tbb/task_scheduler_init.h"
#endif

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <thrust/sequence.h>
#include <thrust/execution_policy.h>
#include <thrust/transform.h>
#include <thrust/tuple.h>
#include <thrust/pair.h>
#include <thrust/unique.h>
#include <thrust/copy.h>
#include <thrust/tabulate.h>
#include <thrust/sequence.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/functional.h>
#include <thrust/for_each.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>
#include <thrust/extrema.h>
#include <thrust/swap.h>

#include <iostream>
#include <stdio.h>
#include <cfloat>
#include <utility>
#include <vector>
#include <algorithm>

#include "diagram_reader.h"


namespace hera {

    template<class Real = double>
    __host__ __device__
    Real get_infinity()
    {
        return -1.0;
    }

    template<class Real = double>
    __host__ __device__
    bool is_infinity(const Real p)
    {
        return p == -1.0;
    }

    template<class Real = double>
    struct AuctionParams {
        Real wasserstein_power { 1.0 };
        Real delta { 0.01 };
        Real internal_p { get_infinity<Real>() };
        Real initial_epsilon { 0.0 };
        Real epsilon { 0.0 };
        Real epsilon_common_ratio { 0.0 };
        int max_phase_num { std::numeric_limits<int>::max() };
        Real gamma_threshold { 0.0 };
        size_t max_bids_per_round { std::numeric_limits<size_t>::max() };
        int k_max_nn { 100 };
    };

} // hera


//#include "dnn/geometry/euclidean-fixed.h"
//#include "dnn/local/kd-tree.h"

//using DnnPoint       = hera::ws::dnn::Point<2, double>;
//using DnnTraits      = hera::ws::dnn::PointTraits<DnnPoint>;


using namespace hera;

using DiagramPoint = thrust::tuple<double, double, int>;

using Bid = thrust::tuple<int, double, int>; // item, bid_value, bid

constexpr double k_max_relative_error = 2.0;
constexpr int k_invalid_idx = -1;
constexpr int k_normal_pt = 1;
constexpr int k_diagonal_pt = 0;


__host__ __device__
double get_cost_q_2(const DiagramPoint& a, const DiagramPoint& b)
{
    if (thrust::get<2>(a) == thrust::get<2>(b) and thrust::get<2>(a) == k_diagonal_pt) {
        return 0.0;
    } else {
        double x_a = thrust::get<0>(a); double y_a = thrust::get<1>(a);
        double x_b = thrust::get<0>(b); double y_b = thrust::get<1>(b);
        double dist = thrust::max(fabs(x_a - x_b), fabs(y_a - y_b));
        return dist * dist;
    }
}


__host__ __device__
double get_cost_q_1(const DiagramPoint& a, const DiagramPoint& b)
{
    if (thrust::get<2>(a) == thrust::get<2>(b) and thrust::get<2>(a) == k_diagonal_pt) {
        //printf("d = 0.0\n");
        return 0.0;
    } else {
        double x_a = thrust::get<0>(a); double y_a = thrust::get<1>(a);
        double x_b = thrust::get<0>(b); double y_b = thrust::get<1>(b);
        //printf("a = (%f, %f), b = (%f, %f), d = %f\n", x_a, y_a, x_b, y_b, thrust::max(fabs(x_a - x_b), fabs(y_a - y_b)));
        return thrust::max(fabs(x_a - x_b), fabs(y_a - y_b));
    }
}


struct is_unassigned_functor : public thrust::unary_function<int, bool>
{
    __device__
    bool operator()(const int b2i)
    {
        return b2i == k_invalid_idx;
    }
};

struct edge_cost_functor : public thrust::unary_function<thrust::tuple<int, int>, double>
{
    thrust::device_ptr<DiagramPoint> bidders;
    thrust::device_ptr<DiagramPoint> items;
    int k_max_nn;

    __host__ __device__
    edge_cost_functor(thrust::device_ptr<DiagramPoint> _bidders, thrust::device_ptr<DiagramPoint> _items, int _k_max_nn) :
        bidders(_bidders),
        items(_items),
        k_max_nn(_k_max_nn)
    {
    }

    __device__
    double operator()(const thrust::tuple<int, int>& idx_pair)
    {
        int b_idx = thrust::get<0>(idx_pair) / k_max_nn;
        int i_idx = thrust::get<1>(idx_pair);
        //printf("b_idx = %d, i_idx = %d\n", b_idx, i_idx);
        //printf("b_idx = %d, i_idx = %d, bidder.x = %f, bidder.y = %f, item.x = %f, item.y = %f\n",
        //        b_idx,
        //        i_idx,
        //        thrust::get<0>(*(bidders + b_idx)),
        //        thrust::get<1>(*(bidders + b_idx)),
        //        thrust::get<0>(*(items + i_idx)),
        //        thrust::get<1>(*(items + i_idx)));
        return get_cost_q_1(bidders[b_idx], items[i_idx]);
    }
};


struct edge_cost_functor_1 : public thrust::unary_function<thrust::tuple<int, int>, double>
{
    thrust::device_ptr<DiagramPoint> bidders;
    thrust::device_ptr<DiagramPoint> items;

    __host__ __device__
    edge_cost_functor_1(thrust::device_ptr<DiagramPoint> _bidders, thrust::device_ptr<DiagramPoint> _items) :
        bidders(_bidders),
        items(_items)
    {
    }

    __device__
    double operator()(const thrust::tuple<int, int>& idx_pair)
    {
        int b_idx = thrust::get<0>(idx_pair);
        int i_idx = thrust::get<1>(idx_pair);
        //printf("b_idx = %d, i_idx = %d\n", b_idx, i_idx);
        //printf("b_idx = %d, i_idx = %d, bidder.x = %f, bidder.y = %f, item.x = %f, item.y = %f\n",
        //        b_idx,
        //        i_idx,
        //        thrust::get<0>(*(bidders + b_idx)),
        //        thrust::get<1>(*(bidders + b_idx)),
        //        thrust::get<0>(*(items + i_idx)),
        //        thrust::get<1>(*(items + i_idx)));
        return get_cost_q_1(bidders[b_idx], items[i_idx]);
    }
};


// to find admissible edges for normal bidders we need vector of distances from bidder
// to all normal items
struct edge_cost_pair_functor : public thrust::unary_function<int, thrust::pair<double, int>>
{
    DiagramPoint bidder;
    thrust::device_ptr<DiagramPoint> items;

    __host__ __device__
    edge_cost_pair_functor(const DiagramPoint _bidder, thrust::device_ptr<DiagramPoint> _items) :
        bidder(_bidder),
        items(_items)
    {
    }

    __device__
    thrust::pair<double, int> operator()(int item_idx)
    {
        //printf("b_idx = %d, i_idx = %d\n", b_idx, i_idx);
        //printf("b_idx = %d, i_idx = %d, bidder.x = %f, bidder.y = %f, item.x = %f, item.y = %f\n",
        //        b_idx,
        //        i_idx,
        //        thrust::get<0>(*(bidders + b_idx)),
        //        thrust::get<1>(*(bidders + b_idx)),
        //        thrust::get<0>(*(items + i_idx)),
        //        thrust::get<1>(*(items + i_idx)));
        return thrust::make_pair(get_cost_q_1(bidder, items[item_idx]), item_idx);
    }
};


// to find admissible edges for diagonal bidders we need coordinates of
// diagonal items
struct diag_coord_idx_pair_functor : public thrust::unary_function<int, thrust::pair<double, int>>
{
    thrust::device_ptr<DiagramPoint> items;

    __host__ __device__
    diag_coord_idx_pair_functor(thrust::device_ptr<DiagramPoint> _items) :
        items(_items)
    {
    }

    __device__
    thrust::pair<double, int> operator()(int item_idx)
    {
        //printf("b_idx = %d, i_idx = %d\n", b_idx, i_idx);
        //printf("b_idx = %d, i_idx = %d, bidder.x = %f, bidder.y = %f, item.x = %f, item.y = %f\n",
        //        b_idx,
        //        i_idx,
        //        thrust::get<0>(*(bidders + b_idx)),
        //        thrust::get<1>(*(bidders + b_idx)),
        //        thrust::get<0>(*(items + i_idx)),
        //        thrust::get<1>(*(items + i_idx)));
        DiagramPoint item = items[item_idx];
        double x_coord = thrust::get<0>(item);
        return thrust::make_pair(x_coord, item_idx);
    }
};


struct edge_value_functor : public thrust::binary_function<int, int, double>
{
    thrust::device_ptr<DiagramPoint> bidders;
    thrust::device_ptr<DiagramPoint> items;
    thrust::device_ptr<double> prices;

    __host__ __device__
    edge_value_functor(thrust::device_ptr<DiagramPoint> _bidders, thrust::device_ptr<DiagramPoint> _items, thrust::device_ptr<double> _prices) :
        bidders(_bidders),
        items(_items),
        prices(_prices)
    {
    }

    __device__
    double operator()(const int& i_idx, const int& b_idx)
    {
        double price = prices[i_idx];
        return get_cost_q_1(bidders[b_idx], items[i_idx]) + price;
    }
};



__host__
double get_max_val(thrust::device_vector<DiagramPoint>& bidders,
                   thrust::device_vector<DiagramPoint>& items,
                   thrust::device_vector<int>& admissible_edges,
                   const int k_max_nn)
{
    std::cout << "Entered get_max_val" << std::endl;
    thrust::counting_iterator<int> cnt_iter_begin = thrust::make_counting_iterator(0);
    thrust::counting_iterator<int> cnt_iter_end = thrust::make_counting_iterator<int>((int)(admissible_edges.size()));
    auto cnt_item_iter_begin = thrust::make_zip_iterator(thrust::make_tuple(cnt_iter_begin, admissible_edges.begin()));
    auto cnt_item_iter_end = thrust::make_zip_iterator(thrust::make_tuple(cnt_iter_end, admissible_edges.end()));
    return thrust::transform_reduce(thrust::device,
            cnt_item_iter_begin,
            cnt_item_iter_end,
            edge_cost_functor(bidders.data(), items.data(), k_max_nn),
            0.0,
            thrust::maximum<double>());
}


using bid_helper_struct = thrust::tuple<int, double, int, double>;


struct bid_helper_reduce_functor : public thrust::binary_function<bid_helper_struct, bid_helper_struct, bid_helper_struct>
{
    __device__
    bid_helper_struct operator()(const bid_helper_struct& a, const bid_helper_struct& b)
    {
        int best_item_idx;
        double best_value;

        int second_best_item_idx;
        double second_best_value;

        double a_best_value = thrust::get<1>(a);
        double b_best_value = thrust::get<1>(b);
        if (a_best_value < b_best_value) {
            best_item_idx = thrust::get<0>(a);
            best_value = a_best_value;
            double a_second_best_value = thrust::get<3>(a);
            if (a_second_best_value < b_best_value) {
                second_best_item_idx = thrust::get<2>(a);
                second_best_value = a_second_best_value;
            } else {
                second_best_item_idx = thrust::get<0>(b);
                second_best_value = b_best_value;
            }
        } else {
            best_item_idx = thrust::get<0>(b);
            best_value = b_best_value;
            double b_second_best_value = thrust::get<3>(b);
            if (b_second_best_value < a_best_value) {
                second_best_item_idx = thrust::get<2>(b);
                second_best_value = b_second_best_value;
            } else {
                second_best_item_idx = thrust::get<0>(a);
                second_best_value = a_best_value;
            }
        }
        return thrust::make_tuple(best_item_idx, best_value, second_best_item_idx, second_best_value);
    }
};


struct bid_helper_transform_functor : public thrust::unary_function<int, bid_helper_struct>
{
    DiagramPoint bidder;
    thrust::device_ptr<double> prices;
    thrust::device_ptr<DiagramPoint> items;
    thrust::device_ptr<int> admissible_edges;
    const int k_invalid_idx;

    __device__
    bid_helper_transform_functor(const DiagramPoint& _bidder,
            thrust::device_ptr<double> _prices,
            thrust::device_ptr<DiagramPoint> _items,
            thrust::device_ptr<int> _admissible_edges,
            const int _k_invalid_idx
            ) :
        bidder(_bidder),
        prices(_prices),
        items(_items),
        admissible_edges(_admissible_edges),
        k_invalid_idx(_k_invalid_idx)
    {
    };


    __device__
    bid_helper_struct operator()(const int i)
    {
        int best_item_idx = admissible_edges[i];
        double price = prices[best_item_idx];
        double best_value = get_cost_q_1(bidder, items[best_item_idx]) + price;
        int second_best_item_idx = k_invalid_idx;
        double second_best_value = DBL_MAX;

        return thrust::make_tuple(best_item_idx, best_value, second_best_item_idx, second_best_value);
    }
};

struct bid_functor
{
    thrust::device_ptr<double> prices;
    thrust::device_ptr<DiagramPoint> bidders;
    thrust::device_ptr<DiagramPoint> items;
    thrust::device_ptr<int> admissible_edges;
    const int k_max_nn;
    const int k_invalid_idx;
    const double epsilon;

    __host__ __device__
    bid_functor(thrust::device_ptr<double> _prices,
            thrust::device_ptr<DiagramPoint> _bidders,
            thrust::device_ptr<DiagramPoint> _items,
            thrust::device_ptr<int> _admissible_edges,
            int _k_max_nn,
            int _k_invalid_idx,
            double _epsilon
            ) :
        prices(_prices),
        bidders(_bidders),
        items(_items),
        admissible_edges(_admissible_edges),
        k_max_nn(_k_max_nn),
        k_invalid_idx(_k_invalid_idx),
        epsilon(_epsilon)
    {
    }

    __device__
    Bid operator()(const int bidder_idx)
    {
        int best_item_idx;
        int second_best_item_idx;

        double best_value = DBL_MAX;
        double second_best_value = DBL_MAX;

        int start_idx = bidder_idx * k_max_nn;
        int end_idx = (bidder_idx + 1) * k_max_nn;

        //  thrust simplest version

        //thrust::device_ptr<int> admissible_edges_begin = admissible_edges + start_idx;
        //thrust::device_ptr<int> admissible_edges_end = admissible_edges + end_idx;

        //thrust::constant_iterator<int> const_bidder_idx_iter(bidder_idx);


        //thrust::transform(thrust::device,
        //        admissible_edges_begin,
        //        admissible_edges_end,
        //        const_bidder_idx_iter,
        //        cost_vec,
        //        edge_value_functor(bidders, items, prices));

        ////for(int i = 0; i < k_max_nn; ++i) {
        ////    double curr_val = cost_vec[i];
        ////    printf("thrust: bidder_idx = %d, i = %d, value[i] = %f\n", bidder_idx, i, curr_val);
        ////}

        //thrust::device_ptr<double> min_ptr = thrust::min_element(thrust::device, cost_vec, cost_vec + k_max_nn);
        //int min_position = min_ptr - cost_vec;
        //best_item_idx = admissible_edges[min_position];

        //best_value = *min_ptr;
        //double last_val = cost_vec[k_max_nn - 1];
        //*min_ptr = last_val;

        //thrust::device_ptr<double> second_min_ptr = thrust::min_element(thrust::device, cost_vec, cost_vec + (k_max_nn - 1));

        //second_best_value = *second_min_ptr;

        //int second_min_position = second_min_ptr - cost_vec;
        //second_best_item_idx = admissible_edges[second_min_position];

        //  -- thrust simplest version

        // thrust - transform-reduce verion

        DiagramPoint bidder = bidders[bidder_idx];
        bid_helper_struct init_value = thrust::make_tuple(k_invalid_idx, DBL_MAX, k_invalid_idx, DBL_MAX);

        bid_helper_struct bid_helper = thrust::transform_reduce(thrust::device,
                thrust::make_counting_iterator(start_idx),
                thrust::make_counting_iterator(end_idx),
                bid_helper_transform_functor(bidder,
                    prices,
                    items,
                    admissible_edges,
                    k_invalid_idx),
                init_value,
                bid_helper_reduce_functor());

        best_item_idx = thrust::get<0>(bid_helper);
        best_value = thrust::get<1>(bid_helper);
        second_best_value = thrust::get<3>(bid_helper);

        // -- thrust - transform-reduce verion

        //  brute force, non-thrust version
        bool use_brute_force = false;
        if (use_brute_force) {
            int brute_force_best_item_idx;
            int brute_force_second_best_item_idx;

            double brute_force_best_value = DBL_MAX;
            double brute_force_second_best_value = DBL_MAX;


            for(int i = start_idx; i < end_idx; ++i) {
                int item_idx = admissible_edges[i];
                double curr_val = get_cost_q_1(bidders[bidder_idx], items[item_idx]) + prices[item_idx];
                //printf("for-loop: bidder_idx = %d, i = %d, value[i] = %f\n", bidder_idx, i, curr_val);
                if (curr_val < brute_force_best_value) {
                    brute_force_best_value = curr_val;
                    brute_force_best_item_idx = item_idx;
                }
            }

            for(int i = start_idx; i < end_idx; ++i) {
                int item_idx = admissible_edges[i];
                if (item_idx == brute_force_best_item_idx) {
                    continue;
                }
                double curr_val = get_cost_q_1(bidders[bidder_idx], items[item_idx]) + prices[item_idx];
                if (curr_val < brute_force_second_best_value) {
                    brute_force_second_best_value = curr_val;
                    brute_force_second_best_item_idx = item_idx;
                }
            }



            assert( fabs(best_value - brute_force_best_value) < 0.000001);
            assert( fabs(second_best_value - brute_force_second_best_value) < 0.000001);
        }

        // -- brute force, non-thrust version

        //printf("min_val = %f, true answer = %f, best_idx = %d, true answer = %d,  second_best_value = %f, true answer = %f\n",
        //        best_value,
        //        brute_force_best_value,
        //        best_item_idx,
        //        brute_force_best_item_idx,
        //        second_best_value,
        //        brute_force_second_best_value);

        double price = prices[best_item_idx];
        double bid_value = second_best_value - best_value + price + epsilon;

        return thrust::make_tuple(best_item_idx, bid_value, bidder_idx);


    }
};


struct compare_bids_by_item_index_functor : thrust::binary_function<Bid, Bid, bool>
{
    __device__
    bool operator()(const Bid& bid_1, const Bid& bid_2)
    {
        return thrust::get<0>(bid_1) == thrust::get<0>(bid_2);
    }
};


struct assignment_functor
{

    thrust::device_ptr<int> bidders_to_items;
    thrust::device_ptr<int> items_to_bidders;
    thrust::device_ptr<double> prices;
    const int k_invalid_idx;

    __host__ __device__
    assignment_functor(thrust::device_ptr<int> _b2i,
                       thrust::device_ptr<int> _i2b,
                       thrust::device_ptr<double>  _prices,
                       const int _k_invalid_idx) :
        bidders_to_items(_b2i),
        items_to_bidders(_i2b),
        prices(_prices),
        k_invalid_idx(_k_invalid_idx)
    {
    }

    __device__
    void operator()(const Bid& bid)
    {

        int item_idx = thrust::get<0>(bid);
        double new_price = thrust::get<1>(bid);
        int bidder_idx = thrust::get<2>(bid);
        int old_owner_idx = items_to_bidders[item_idx];

        bidders_to_items[bidder_idx] = item_idx;
        items_to_bidders[item_idx] = bidder_idx;

        if (old_owner_idx !=  k_invalid_idx) {
            // old owner becomes unassigned
            bidders_to_items[old_owner_idx] = k_invalid_idx;
        }

        prices[item_idx] = new_price;
    }
};



__host__
double get_wassserstein_cost(const int num_total_pts,
            thrust::device_vector<DiagramPoint>& bidders,
            thrust::device_vector<DiagramPoint>& items,
            thrust::device_vector<int>& bidders_to_items,
            thrust::device_vector<int>& items_to_bidders,
            thrust::device_vector<int>& admissible_edges,
            thrust::device_vector<double>& prices,
            AuctionParams<double>& params
        )
{

    thrust::device_vector<int> unassigned_bidders(num_total_pts);

    thrust::fill(thrust::device, bidders_to_items.begin(), bidders_to_items.end(), k_invalid_idx);
    thrust::fill(thrust::device, items_to_bidders.begin(), items_to_bidders.end(), k_invalid_idx);

    int k_max_nn = params.k_max_nn;

    double epsilon;
    if (0.0 == params.initial_epsilon) {
        double max_val = get_max_val(bidders, items, admissible_edges, k_max_nn);
        printf("max_val = %f\n", max_val);
        epsilon =  max_val / 4.0;
    } else {
        epsilon = params.initial_epsilon;
    }

    if (params.epsilon_common_ratio == 0.0) {
        params.epsilon_common_ratio = 5.0;
    }

    double current_cost;

    for(int eps_scaling_phase = 0; eps_scaling_phase < 50; ++eps_scaling_phase) {

        while(not unassigned_bidders.empty()) {

            // get unassigned bidders
            auto unassigned_end = thrust::copy_if(thrust::device,
                    thrust::make_counting_iterator<int>(0),
                    thrust::make_counting_iterator<int>(num_total_pts),
                    bidders_to_items.begin(),
                    unassigned_bidders.begin(),
                    is_unassigned_functor());

            unassigned_bidders.resize(thrust::distance(unassigned_bidders.begin(), unassigned_end));
            //printf("unassigned_bidders.size = %d\n", (int)unassigned_bidders.size());

            thrust::device_vector<Bid> bid_table(unassigned_bidders.size());

            thrust::transform(thrust::device,
                    unassigned_bidders.begin(),
                    unassigned_end,
                    bid_table.begin(),
                    bid_functor(prices.data(),
                        bidders.data(),
                        items.data(),
                        admissible_edges.data(),
                        k_max_nn,
                        k_invalid_idx,
                        epsilon
                        ));

            thrust::sort(thrust::device,
                    bid_table.begin(),
                    bid_table.end(),
                    thrust::greater<Bid>());


            auto bid_table_end = thrust::unique(thrust::device,
                    bid_table.begin(),
                    bid_table.end(),
                    compare_bids_by_item_index_functor());

            bid_table.resize(thrust::distance(bid_table.begin(), bid_table_end));

            //thrust::host_vector<Bid> bid_table_copy(bid_table);
            //for(int i = 0; i < bid_table_copy.size(); ++i) {
            //    printf("i = %d, item_idx = %d, bid_value = %f, bidder_idx = %d\n",
            //            i,
            //            thrust::get<0>(bid_table_copy[i]),
            //            thrust::get<1>(bid_table_copy[i]),
            //            thrust::get<2>(bid_table_copy[i])
            //          );
            //}

            thrust::for_each(thrust::device,
                    bid_table.begin(),
                    bid_table_end,
                    assignment_functor(bidders_to_items.data(),
                        items_to_bidders.data(),
                        prices.data(),
                        k_invalid_idx));

            //thrust::host_vector<double> prices_copy(prices);
            //thrust::host_vector<int> bidders_to_items_copy(bidders_to_items);
            //thrust::host_vector<int> items_to_bidders_copy(items_to_bidders);

            //for(int i = 0; i < num_total_pts; ++i) {
            //    printf("i = %d,  prices[i] = %f,  b2i[i] = %d, i2b[i] = %d\n",
            //            i,
            //            prices_copy[i],
            //            bidders_to_items_copy[i],
            //            items_to_bidders_copy[i]
            //          );
            //}

        }

        thrust::counting_iterator<int> bidder_cnt_iter_begin = thrust::make_counting_iterator<int>(0);
        thrust::counting_iterator<int> bidder_cnt_iter_end = thrust::make_counting_iterator<int>(num_total_pts);
        auto zip_iter_begin = thrust::make_zip_iterator(thrust::make_tuple(bidder_cnt_iter_begin, bidders_to_items.begin()));
        auto zip_iter_end = thrust::make_zip_iterator(thrust::make_tuple(bidder_cnt_iter_end, bidders_to_items.end()));

        double relative_error;

        current_cost = thrust::transform_reduce(thrust::device,
                zip_iter_begin,
                zip_iter_end,
                edge_cost_functor_1(bidders.data(), items.data()),
                0.0,
                thrust::plus<double>());

        double reduced_cost = current_cost - num_total_pts * epsilon;
        if (reduced_cost > 0.0) {
            double denominator = pow(reduced_cost, 1.0 / params.wasserstein_power);
            double numerator = pow(current_cost, 1.0 / params.wasserstein_power) - pow(reduced_cost, 1.0 / params.wasserstein_power);
            relative_error = numerator / denominator;
        } else {
            relative_error = k_max_relative_error;
        }

        printf("current_cost = %f, relative_error = %f\n", current_cost, relative_error);

        if (relative_error <= params.delta) {
            break;
        }

        // flush assignment
        thrust::fill(thrust::device, bidders_to_items.begin(), bidders_to_items.end(), k_invalid_idx);
        thrust::fill(thrust::device, items_to_bidders.begin(), items_to_bidders.end(), k_invalid_idx);

        unassigned_bidders = thrust::device_vector<int>(num_total_pts);
        thrust::sequence(thrust::device, unassigned_bidders.begin(), unassigned_bidders.end(), 0);

        epsilon /= params.epsilon_common_ratio;

    }
    params.epsilon = epsilon;
    return current_cost;

}

int main(int argc, char* argv[])
{

#ifndef __CUDACC__
    tbb::task_scheduler_init init(1);
#endif

    using PairVector = std::vector<std::pair<double, double>>;
    PairVector diagram_A, diagram_B;

    AuctionParams<double> params;

    if (argc < 3 ) {
        std::cerr << "Usage: " << argv[0] << " file1 file2 [wasserstein_power] [relative_error] [internal norm] [initial epsilon] [epsilon_factor] [max_bids_per_round] [gamma_threshold] [k_max_nn]. By default power is 1.0, relative error is 0.01, internal norm is l_infinity, initall epsilon is chosen automatically, epsilon factor is 5.0, Jacobi variant is used (max bids per round is maximal), gamma_threshold = 0.0." << std::endl;
        return 1;
    }

    if (not hera::read_diagram_point_set<double, PairVector>(argv[1], diagram_A)) {
        std::exit(1);
    }

    if (not hera::read_diagram_point_set<double, PairVector>(argv[2], diagram_B)) {
        std::exit(1);
    }

    params.wasserstein_power = (4 <= argc) ? atof(argv[3]) : 1.0;
    if (params.wasserstein_power < 1.0) {
        std::cerr << "The third argument (wasserstein_degree) was \"" << argv[3] << "\", must be a number >= 1.0. Cannot proceed. " << std::endl;
        std::exit(1);
    }

    if (params.wasserstein_power == 1.0) {
        hera::remove_duplicates<double, PairVector>(diagram_A, diagram_B);
    }

    //default relative error:  1%
    params.delta = (5 <= argc) ? atof(argv[4]) : 0.01;
    if ( params.delta <= 0.0) {
        std::cerr << "The 4th argument (relative error) was \"" << argv[4] << "\", must be a number > 0.0. Cannot proceed. " << std::endl;
        std::exit(1);
    }

    // default for internal metric is l_infinity
    params.internal_p = ( 6 <= argc ) ? atof(argv[5]) : get_infinity<double>();
    if (std::isinf(params.internal_p)) {
        params.internal_p = get_infinity<double>();
    }

   // for epsilon-scaling
    params.initial_epsilon= ( 7 <= argc ) ? atof(argv[6]) : 0.0 ;

    if (params.initial_epsilon < 0.0) {
        std::cerr << "The 6th argument (initial epsilon) was \"" << argv[6] << "\", must be a non-negative number. Cannot proceed." << std::endl;
        std::exit(1);
    }

    params.epsilon_common_ratio = ( 8 <= argc ) ? atof(argv[7]) : 0.0 ;
    if (params.epsilon_common_ratio <= 1.0 and params.epsilon_common_ratio != 0.0) {
        std::cerr << "The 7th argument (epsilon factor) was \"" << argv[7] << "\", must be a number greater than 1. Cannot proceed." << std::endl;
        std::exit(1);
    }


    params.max_bids_per_round = ( 9 <= argc ) ? atoi(argv[8]) : 0;
    if (params.max_bids_per_round == 0)
        params.max_bids_per_round = std::numeric_limits<size_t>::max();


    params.gamma_threshold = (10 <= argc) ? atof(argv[9]) : 0.0;

    params.max_phase_num = 800;

    int num_total_pts = diagram_A.size() + diagram_B.size();

    params.k_max_nn = ( 11 <= argc ) ? atoi(argv[10]) : num_total_pts;

    int sparse_k_max_nn = (12 <= argc) ? atoi(argv[11]) : params.k_max_nn / 2;

    thrust::host_vector<DiagramPoint> bidders_host(num_total_pts);
    thrust::host_vector<DiagramPoint> items_host(num_total_pts);

    int num_normal_bidders = diagram_A.size();
    int num_normal_items = diagram_B.size();
    int num_diag_bidders = num_normal_items;
    int num_diag_items = num_normal_bidders;

    for(int i = 0; i < num_normal_bidders; ++i) {
        bidders_host[i] = thrust::make_tuple(diagram_A[i].first, diagram_A[i].second, k_normal_pt);
        double diag_coord = (diagram_A[i].first + diagram_A[i].second) / 2;
        items_host[i] = thrust::make_tuple(diag_coord, diag_coord, k_diagonal_pt);
    }

    for(int i = num_normal_bidders; i < num_total_pts; ++i) {
        int idx = i - num_normal_bidders;
        items_host[i] = thrust::make_tuple(diagram_B[idx].first, diagram_B[idx].second, k_normal_pt);
        double diag_coord = (diagram_B[idx].first + diagram_B[idx].second) / 2;
        bidders_host[i] = thrust::make_tuple(diag_coord, diag_coord, k_diagonal_pt);
    }


    thrust::device_vector<DiagramPoint> bidders(bidders_host);
    thrust::device_vector<DiagramPoint> items(items_host);

    //const int k_max_nn = (num_total_pts < 10) ? num_total_pts : 10;


    // get admissible edges
    //// -------------------------------------------
    //// brute-fore non-parallel version
    //std::vector<int> admissible_edges_std(k_max_nn*num_total_pts, k_invalid_idx);

    //// for normal bidders
    //for(int i = 0; i < num_normal_bidders; ++i) {
    //    DiagramPoint bidder = bidders_host[i];
    //    std::vector<std::pair<double, int>> dist_idx_vec(num_normal_items);
    //    for(int j = num_diag_items; j < num_total_pts; ++j) {
    //        std::pair<double, int> p;
    //        DiagramPoint item = items_host[j];
    //        p.first = get_cost_q_1(bidder, item);
    //        p.second = j;
    //        dist_idx_vec[j - num_diag_items] = p;
    //    }
    //    std::sort(dist_idx_vec.begin(), dist_idx_vec.end());
    //    //for(const auto p : dist_idx_vec) {
    //    //    std::cout << i << " (" << p.first << ", " << p.second << ")" << std::endl;
    //    //}
    //    int begin_idx = i * k_max_nn;
    //    admissible_edges_std[begin_idx] = i;
    //    begin_idx++;
    //    for(int k = 0; k < k_max_nn; ++k) {
    //        admissible_edges_std[begin_idx + k] = dist_idx_vec[k].second;
    //    }
    //}

    //printf("normal bidders done\n");

    ////for(const auto t : admissible_edges_std) {
    ////    printf("%d\n", t);
    ////}

    //std::vector<std::pair<double, int>> item_diag_coord_vec(num_diag_items);

    //for(int i = 0; i < num_diag_items; ++i) {
    //    DiagramPoint item = items_host[i];
    //    item_diag_coord_vec[i] = std::make_pair(thrust::get<0>(item), i);
    //}

    //std::sort(item_diag_coord_vec.begin(), item_diag_coord_vec.end());

    //for(int i = num_normal_bidders; i < num_total_pts; ++i) {
    //    DiagramPoint bidder = bidders_host[i];
    //    auto iter = std::lower_bound(item_diag_coord_vec.begin(),
    //            item_diag_coord_vec.end(),
    //            std::make_pair(thrust::get<0>(bidder), k_invalid_idx));

    //    int ae_idx = i * k_max_nn;
    //    // pre-projection is the only normal item diagonal bidder i can be
    //    // connected to
    //    admissible_edges_std[ae_idx] = i;
    //    for(int j = 0; j < k_max_nn - 1; ++j) {
    //        if (iter == item_diag_coord_vec.end()) {
    //            iter = item_diag_coord_vec.begin();
    //        }
    //        admissible_edges_std[++ae_idx] = iter->second;
    //        ++iter;
    //    }
    //}

    //printf("diagonal bidders done\n");

    ////for(const auto t : admissible_edges_std) {
    ////    printf("%d\n", t);
    ////}

    //// -------------------------------------------


    // nearest neighbours - brute-force parallel
    // -------------------------------------------

    int k_max_nn = params.k_max_nn;

    std::vector<int> admissible_edges_std(k_max_nn*num_total_pts, k_invalid_idx);

    // for normal bidders
    for(int i = 0; i < num_normal_bidders; ++i) {
        DiagramPoint bidder = bidders_host[i];
        thrust::device_vector<thrust::pair<double, int>> dist_idx_vec(num_normal_items);
        thrust::transform(thrust::device,
                thrust::make_counting_iterator<int>(num_diag_items),
                thrust::make_counting_iterator<int>(num_total_pts),
                dist_idx_vec.begin(),
                edge_cost_pair_functor(bidder, items.data()));

        thrust::sort(thrust::device, dist_idx_vec.begin(), dist_idx_vec.end());
        thrust::host_vector<thrust::pair<double, int>> dist_idx_vec_host(dist_idx_vec);
        int begin_idx = i * k_max_nn;
        admissible_edges_std[begin_idx] = i;
        begin_idx++;
        for(int k = 0; k < k_max_nn-1; ++k) {
            admissible_edges_std[begin_idx + k] = dist_idx_vec_host[k].second;
        }
    }

    printf("normal bidders done\n");

    //for(const auto t : admissible_edges_std) {
    //    printf("%d\n", t);
    //}

    thrust::device_vector<thrust::pair<double, int>> item_diag_coord_vec(num_diag_items);

    thrust::transform(thrust::device,
                thrust::make_counting_iterator<int>(0),
                thrust::make_counting_iterator<int>(num_diag_items),
                item_diag_coord_vec.begin(),
                diag_coord_idx_pair_functor(items.data()));


    thrust::sort(thrust::device, item_diag_coord_vec.begin(), item_diag_coord_vec.end());

    thrust::host_vector<thrust::pair<double, int>> item_diag_coord_vec_host(item_diag_coord_vec);

    for(int i = num_normal_bidders; i < num_total_pts; ++i) {
        DiagramPoint bidder = bidders_host[i];
        auto iter = std::lower_bound(item_diag_coord_vec_host.begin(),
                item_diag_coord_vec_host.end(),
                thrust::make_pair(thrust::get<0>(bidder), k_invalid_idx));

        int ae_idx = i * k_max_nn;
        // pre-projection is the only normal item diagonal bidder i can be
        // connected to
        admissible_edges_std[ae_idx] = i;
        for(int j = 0; j < k_max_nn - 1; ++j) {
            if (iter == item_diag_coord_vec_host.end()) {
                iter = item_diag_coord_vec_host.begin();
            }
            admissible_edges_std[++ae_idx] = thrust::get<1>(*iter);
            ++iter;
        }
    }

    printf("diagonal bidders done\n");

    //for(const auto t : admissible_edges_std) {
    //    printf("%d\n", t);
    //}

    // -------------------------------------------

    AuctionParams<double> params_sparse(params);
    params_sparse.k_max_nn = sparse_k_max_nn;

    std::vector<int> admissible_edges_std_sparse(params_sparse.k_max_nn * num_total_pts);

    for(int i = 0; i < num_total_pts; ++i) {
        int long_start_idx = i * params.k_max_nn;
        int short_start_idx = i * params_sparse.k_max_nn;
        for(int j = 0; j < params_sparse.k_max_nn; ++j) {
            admissible_edges_std_sparse[short_start_idx + j] = admissible_edges_std[long_start_idx + j];
        }
    }

    thrust::device_vector<int> admissible_edges(admissible_edges_std);
    thrust::device_vector<int> admissible_edges_sparse(admissible_edges_std_sparse);

    {
    //thrust::host_vector<DiagramPoint> bidders_host;
    //bidders_host = bidders;
    //thrust::host_vector<DiagramPoint> items_host = items;

    //std::vector<size_t> kdtree_items_(num_normal_items, k_invalid_idx);
    //std::vector<DnnPoint> dnn_points;
    //std::vector<DnnPoint*> dnn_point_handles;
    //size_t dnn_item_idx { 0 };
    //size_t true_idx { 0 };
    //dnn_points.reserve(diagram_B.size());
    //// store normal items in kd-tree
    //for(int i = num_normal_bidders; i < num_total_pts; ++i) {
    //    kdtree_items_[true_idx] = dnn_item_idx;
    //    // index of items is id of dnn-point
    //    DnnPoint p(true_idx);
    //    DiagramPoint g = items[i];
    //    p[0] = thrust::get<0>(g);
    //    p[1] = thrust::get<1>(g);
    //    double hhh = p[0];
    //    printf("hhh = %f\n", hhh);
    //    dnn_points.push_back(p);
    //    assert(dnn_item_idx == dnn_points.size() - 1);
    //    dnn_item_idx++;
    //    true_idx++;
    //}
    //assert(dnn_points.size() < items_host.size() );
    //for(size_t i = 0; i < dnn_points.size(); ++i) {
    //    dnn_point_handles.push_back(&dnn_points[i]);
    //}
    //DnnTraits traits;
    ////traits.internal_p = internal_p;
    //hera::ws::dnn::KDTree<DnnTraits> kdtree(traits, dnn_point_handles, params.wasserstein_power);

    ////    // loop over normal bidders, find nearest neighbours
    //for(size_t bidder_idx = 0; bidder_idx < num_normal_bidders; ++bidder_idx) {
    //    DiagramPoint b = bidders_host[bidder_idx];
    //    size_t ae_idx = k_max_nn * bidder_idx;
    //    size_t end_index = k_max_nn * (bidder_idx + 1);
    //    DnnPoint bidder_dnn;
    //    bidder_dnn[0] = thrust::get<0>(b);
    //    bidder_dnn[1] = thrust::get<1>(b);
    //    double ttt = bidder_dnn[0];
    //    printf("ttt = %f\n", ttt);
    //    auto nearest_neighbours = kdtree.findK(bidder_dnn, k_max_nn - 1);
    //    assert(nearest_neighbours.size() == k_max_nn - 1);
    //    for(const auto& x : nearest_neighbours) {
    //        admissible_edges_std[ae_idx++] = x.p->id();
    //    }
    //    admissible_edges_std[ae_idx] = bidder_idx;
    //}
    //for(const auto i : admissible_edges_std) {
    //    std::cout << i << std::endl;
    //}

    ////            // diagonal bidder
    ////            // the only admissible normal item is projection preimage
    ////            admissible_edges_host[ae_idx++] = bidder_idx;
    ////            // add some admissible diagonal items
    ////            size_t diag_bidder_number = bidder_idx - num_normal_bidders;
    ////            assert(num_diag_items > k_max_nn);
    ////            int init_diag_idx = diag_bidder_number - ( k_max_nn / 2);
    ////            for(int i = 0; i < k_max_nn - 1; ++i) {
    ////                size_t diag_item_idx = static_cast<size_t>((i + init_diag_idx) % num_diag_items);
    ////                admissible_edges_host[ae_idx++] = diag_item_idx;
    ////            }
    ////        }
    ////    }
    ////    admissible_edges = admissible_edges_host;
    ////}

    //for(int i = 0; i < k_max_nn * num_total_pts; ++i) {
    //    admissible_edges[i] = i % k_max_nn;
    //}
    }

    thrust::device_vector<int> bidders_to_items(num_total_pts, k_invalid_idx);
    thrust::device_vector<int> items_to_bidders(num_total_pts, k_invalid_idx);

    thrust::device_vector<double> prices(num_total_pts, 0.0);

    thrust::device_vector<int> unassigned_bidders(num_total_pts);


    params.epsilon_common_ratio = 5.0;

    params.delta = 0.01;

    double wasserstein_cost_sparse = get_wassserstein_cost(num_total_pts,
                                                    bidders,
                                                    items,
                                                    bidders_to_items,
                                                    items_to_bidders,
                                                    admissible_edges_sparse,
                                                    prices,
                                                    params_sparse
                                                   );

    params.initial_epsilon = params_sparse.epsilon;

    printf("sparser graph done\n");

    double wasserstein_cost = get_wassserstein_cost(num_total_pts,
                                                    bidders,
                                                    items,
                                                    bidders_to_items,
                                                    items_to_bidders,
                                                    admissible_edges,
                                                    prices,
                                                    params
                                                   );

    printf("distance = %f\n", pow(wasserstein_cost, 1.0 / params.wasserstein_power));

    return 0;
}
